![](custodia_app_logo.png)

# CUSTODIA

Custodia is trying to bring to the world a new generation of parental control and monitoring, it is providing two products: Custodia which is destined for parents and Custodia kids that should be installed on your kids devices.

- CUSTUDIA IS AVAILABLE ON BOTH ANDROID AND IOS

## Features overview

• **Watchman**: Watchman lets you keep track of your child's device status such as: *is the phone on vibration? is ringtone on? is your child's location services currently active ? what's the current battery level? how many hours does my child spend on his phone? what apps is he using the most?*
Watchman will answer those questions for you!

• **Taskmaster**: Teach your children the basics of project and time management, essential keys for grownups life.

• **Messenger**: Chat with your children in real time without going offtrack to other social media platforms.

• **Quiz**: Through a series of predefined quizzes our AI will be able to determine any anxieties, off social behaviors, signs of phone/tablet addiction detection & prevention and many more.

• **Gamecenter**: Gamecenter is a place where you meet your kids for 1v1 mini games such as TIC-TAC-TOE and Chess, solo games are also to be included.

• **Sentry**: Track the live geolocation of your child and add restrictions to where your kid is permitted to go alone.

• **Experience AR**: You or your child can explore the wonders of Augmented Reality worlds.

## Prerequisites
Make sure you have successfully installed following tools on your system before getting started:

- Git: [Navigate through Git](https://opensource.com/article/18/1/step-step-guide-git)
- Deno & Typescript: [Get started with Deno](https://deno.land/#installation)
- MongoDB and its tools: [Get started with MongoDB](https://docs.mongodb.com/manual/)
- Flutter/Dart SDK: [Download and explore Flutter](https://flutter.dev/docs/get-started/install)

To run the project you can use either [VSCODE](https://code.visualstudio.com/Download) or [Android Studio](https://developer.android.com/studio?gclid=Cj0KCQjwh_eFBhDZARIsALHjIKdPsu7hughy5GzLhLjGxu5fD5iyEj9WYxmNOj63Km7-ZBlEMWh8G24aArg1EALw_wcB&gclsrc=aw.ds).

*DO NOT FORGET TO DOWNLOAD AND INSTALL ALL NECESSARY EXTENSIONS AND TOOLS*


## Installation

STEP-001-a [BACKEND -- DOCKER]

```bash
#Clone or download the remote Gitlab repo
#run command
git clone https://gitlab.com/espritmobile-2021/dovahkiin/custodia.git
#then using Docker
#run command
docker compose up -d
```
OR

STEP-001-b [BACKEND -- VANILLA/NO DOCKER]
```bash
#step 1

#Clone or download the remote Gitlab repo
#run command
git clone https://gitlab.com/espritmobile-2021/dovahkiin/custodia.git

#step 2
#nothing is demanded on your part just sit and let Deno import all 
#required modules and dependencies

#step 3
#after making sure that Deno finished all running tasks
#startup the backend services by running the command below
#(leave all flags as they are)
deno run --allow-net --allow-write --allow-read --allow-plugin --unstable server.ts
```

STEP-002 [DATABASE]

```bash
#as a database we are using the NoSQL MongoDB
#run command
brew services start mongodb-community #MacOS
```

STEP-003 [FLUTTER -- CLIENT]

```bash
#to get started firstly
#run command to check that everything is working as it should
flutter doctor
#then run below command to update/import required third-party dependencies/Users/thorvalld/Desktop/snapshot.png
flutter pub get
#finally
flutter run
```

## Snapshot

![](snapshot.png)

## Contributing team

• TEAM DOVAHKIIN - 4SIM2

- Anis BEJAOUI
- Ghassen KAHRI
- Med. Amine NAFFETI

## License
ALL RIGHTS TO CUSTODIA ARE RESERVED TO DOBAHKIIN
