import db from "../db/mongo.ts";
import {RouterContext} from 'https://deno.land/x/oak/mod.ts';

import {transferReward} from "../services/userService.ts";

const taskCollection = db.collection<TaskSchema>("task");
const quizCollection = db.collection<QuizSchema>("quiz");
const questionCollection = db.collection<QuestionSchema>("question");
const assignmentCollection = db.collection<AssignmentSchema>("assignment");

interface AssignmentSchema{
    _id : {$oid : string},
    id : Number,
    id_parent : Number,
    id_child : Number,
    score : Number,
    responses : string,
    status : string,
    reward : Number,
    id_quiz : Number
}

interface QuizSchema{
    _id : {$oid : string},
    id : Number,
    label : string,
    description : string,
}

interface QuestionSchema{
    _id : {$oid : string},
    id : Number,
    correctAnswer : string,
    answers : string,
    id_quiz : Number
}

interface TaskSchema{
    _id: { $oid: string },
    id : Number,
    lable : string,
    description : string,
    category : string,
    reward : number,
    status : number,
    created_at : Date,
    dueDate : string,
    parent_email : string,
    child_email : string
    
}

//////////// TASKS /////////////

//get all parent tasks
const getTasks = async (context : RouterContext) => {
    const mail = await context.params.email;
    const tasks = await taskCollection.find({parent_email : mail }).toArray();
    context.response.body = JSON.stringify(tasks);
    context.response.status = 200;
}

//get all  tasks
const getAllTasks = async (context : RouterContext) => {
    const tasks = await taskCollection.find({ _id: { $ne: null } }).toArray();
    context.response.body = tasks;
    context.response.status = 201;
}

//sort by date
const sortByDate = async (context : RouterContext) => {
    const pid = await Number(context.params.id);
    const tasks  = await taskCollection.find({id_parent : pid}).sort({cearted_at: -1}).toArray();
    context.response.body = tasks[0];
    context.response.status = 201;
}

//get child tasks
const getChildTasks = async (context : RouterContext) => {
    const mail = await context.params.email;
    const tasks = await taskCollection.find({child_email : mail }).toArray();
    context.response.body = JSON.stringify(tasks);
    context.response.status = 200;
}

//get todo tasks

const getTodoTasks = async (context : RouterContext) => {
    const tasks = await taskCollection.find({status : "0%"}).toArray();
    context.response.body = tasks;
    context.response.status = 201;
}

//get doing tasks

const getDoingTasks = async (context : RouterContext) => {
    const tasks = await taskCollection.find({status : "50%"}).toArray();
    context.response.body = tasks;
    context.response.status = 201;
}

//get done tasks

const getDoneTasks = async (context : RouterContext) => {
    const tasks = await taskCollection.find({status : "100%"}).toArray();
    context.response.body = tasks;
    context.response.status = 201;
}

//add task
const addTask = async (context : RouterContext) => {
    const values = await context.request.body().value;
    const parent = await context.params.parent;
    const child = await context.params.child;
    const taskCount = await taskCollection.count({ lable : { $ne: null } });

    taskCollection.insertOne({
        "id" : taskCount+1,
        "lable" : values.lable,
        "description" : values.description,
        "category" : values.category,
        "reward" : values.reward,
        "status" : "0%",
        "created_at" : values.created_at,
        "dueDate" : values.dueDate,
        "parent_email" : parent,
        "child_email" : child,
        "colorVal" : values.colorVal
    });

    context.response.status = 201;
    context.response.body = "added succussfully";
}

const getTaskById = async (context : RouterContext)=>{
    const idTask = Number(context.params.id);
    const task = await taskCollection.find({id : idTask}).toArray();
    context.response.body = task[0];
}

// set task to in progress
const setTaskToOnprogress = async (context : RouterContext) => {

    const taskId = Number(context.params.id);

    const { matchedCount, modifiedCount, upsertedId } = await taskCollection.updateOne({id : taskId}, {$set : {status: "50%"}});
    context.response.body = matchedCount+" "+modifiedCount+" "+upsertedId;
    context.response.status  = 201;
}

//set task to completed
const setTaskToCompleted = async (context : RouterContext) => {

    const taskId = Number(context.params.id);

    //set status to 100
    const { matchedCount, modifiedCount, upsertedId } = 
    await taskCollection.updateOne({id : taskId}, {$set : {status: "100%"}});

    //transfer credits to assigned child
    const task = await taskCollection.findOne({id : taskId});
    
    var mail = task?.child_email;
    var bonus = task?.reward;
    if(mail !== undefined && bonus!== undefined){
        transferReward(mail, bonus);
    }
    
    context.response.body = matchedCount+" "+modifiedCount+" "+upsertedId;
    context.response.status  = 201;
}

//sort parent tasks by status

const sortParetTasksByStatus = async (context : RouterContext) => {
    const parentMail = context.params.mail;

    const tasks = await taskCollection.find({parent_email : parentMail}).sort({status : -1}).toArray();
    context.response.body = tasks;
    context.response.status = 201;

}

//sort parent tasks by status

const sortChildTasksByStatus = async (context : RouterContext) => {
    const parentMail = context.params.parent;
    const childMail = context.params.child;

    const tasks = await taskCollection.find({parent_email : parentMail, child_email : childMail}).sort({status : -1}).toArray();
    context.response.body = tasks;
    context.response.status = 201;

}

const deleteTask = async (context : RouterContext) => {
    const taskId = context.params.id;
    await taskCollection.deleteOne({ id : taskId});

    context.response.body = "deleted succufully !";

}

const getTodoTasksByChild = async (context : RouterContext) => {
    const mail = await context.params.child;
    const tasks = await taskCollection.find({status : "0%", child_email : mail}).toArray();
    context.response.body = tasks;
    context.response.status = 200;
}

const getDoingTasksByChild = async (context : RouterContext) => {
    const mail = await context.params.child;
    const tasks = await taskCollection.find({status : "50%", child_email : mail}).toArray();
    context.response.body = tasks;
    context.response.status = 200;
}

const getDoneTasksByChild = async (context : RouterContext) => {
    const mail = await context.params.child;
    const tasks = await taskCollection.find({status : "100%", child_email : mail}).toArray();
    context.response.body = tasks;
    context.response.status = 200;
}

const getTodoTasksByParent = async (context : RouterContext) => {
    const mail = await context.params.parent;
    const tasks = await taskCollection.find({status : "0%", parent_email : mail}).toArray();
    context.response.body = JSON.stringify(tasks);
    context.response.status = 200;
}

const getDoingTasksByParent = async (context : RouterContext) => {
    const mail = await context.params.parent;
    const tasks = await taskCollection.find({status : "50%", parent_email : mail}).toArray();
    context.response.body = JSON.stringify(tasks);
    context.response.status = 200;
}

const getDoneTasksByParent = async (context : RouterContext) => {
    const mail = await context.params.parent;
    const tasks = await taskCollection.find({status : "100%", parent_email : mail}).toArray();
    context.response.body = JSON.stringify(tasks);
    context.response.status = 200;
}


//////////// QUIZ /////////////
const getAllQuizzes = async (context : RouterContext) => {
    const  quizzes = await quizCollection.find({ label : { $ne: null } }).toArray();
    context.response.body = quizzes;
    context.response.status = 200;
}

//get child quizzes

const getChildQuizzes = async (context : RouterContext) => {
    
    const childId = await Number(context.params.id);
    const assignments = await assignmentCollection.find({id_child : childId}).toArray();
    var quizzes = new Array();

    
    for(let i=0; i<assignments.length; i++){
        const quiz = await quizCollection.find({id : assignments[i].id_quiz}).toArray();
        quizzes.push(quiz[0]) ;
    }
    context.response.body = quizzes;
}

const assignQuizToChild = async (context : RouterContext) => {
    const assCount = await assignmentCollection.count({ id : { $ne: null } });
    const values = await context.request.body().value;
    await assignmentCollection.insertOne({
        "id" : assCount+1,
        "id_child" : values.id_child,
        "score" : null,
        "responses" : null, //list of responses
        "status" : "0%",
        "reward" : values.reward, // points 
        "id_quiz" : values.id_quiz,
        "id_parent" : values.id_parent
    });

    context.response.body = "hey hoe"+ assCount;

}

const getQuizQuestions = async (context : RouterContext) => {
    const quizId = await Number(context.params.id);
    const questions = await questionCollection.find({quiz_id : quizId}).toArray();

    context.response.body = questions;
    context.response.status = 201;
}





//////////// QUESTIONS /////////////



export {
    getChildQuizzes,
    getQuizQuestions,
    assignQuizToChild,
    getAllQuizzes,
    getTasks,
    addTask,
    sortByDate,
    getTaskById,
    getChildTasks,
    setTaskToOnprogress,
    setTaskToCompleted,
    sortParetTasksByStatus,
    sortChildTasksByStatus,
    deleteTask,
    getTodoTasks,
    getDoingTasks,
    getDoneTasks,
    getTodoTasksByChild,
    getDoingTasksByChild,
    getDoneTasksByChild,
    getTodoTasksByParent,
    getDoingTasksByParent,
    getDoneTasksByParent,
    getAllTasks};