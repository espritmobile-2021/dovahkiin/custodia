import { RouterContext } from "https://deno.land/x/oak/mod.ts";
import db from "../db/mongo.ts";
import {withdrawCredits} from "../services/userService.ts";


const rewardCollection = db.collection<RewardSchema>("rewards");

// interface reward
interface RewardSchema{
    _id: { $oid: string },
    title: string;
    price: number;
    parent: string;
}

//all gifts by parent
const fetchGifts = async (ctx : RouterContext) => {
    const body =  ctx.request.body();
    const values = await body.value;

    const gifts = await rewardCollection.find({
        "parent": values.parent
    }).toArray();

    ctx.response.status = 200;
    ctx.response.body = JSON.stringify(gifts);
}

//add reward
const addReward = async (ctx : RouterContext) => {
    const body =  ctx.request.body();
    const values = await body.value;
    const rewardCount = await rewardCollection.count({ title : { $ne: null } });

    await rewardCollection.insertOne({
        "id" : rewardCount+1,
        "title" : values.title,
        "price" : values.price,
        "parent": values.parent
    });

    ctx.response.status = 201;
    ctx.response.body = "gift added to store";
}

//del
const deleteReward = async (ctx : RouterContext) => {
    const body =  ctx.request.body();
    const values = await body.value;

    await rewardCollection.deleteOne({
        "id" : values.id
    });

    ctx.response.status = 201;
    ctx.response.body = "gift deleted";
}

//request reward
const requestReward = async (ctx : RouterContext) => {
    const body =  ctx.request.body();
    const values = await body.value;

    withdrawCredits(values.email, values.credits)

    ctx.response.status = 200;
    ctx.response.body = "you just got a gift";
}

export{addReward, deleteReward, requestReward, fetchGifts}