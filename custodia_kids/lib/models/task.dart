class Task {
  final int idt;
  final String taskTitle;
  final String taskDesc;
  final String taskPoints;
  final String createdAt;
  final String dueDate;
  final String assignedTo;
  final String originalPoster;
  final String taskStatus;

  Task(
      {this.idt,
      this.taskTitle,
      this.taskDesc,
      this.createdAt,
      this.dueDate,
      this.taskPoints,
      this.taskStatus,
      this.originalPoster,
      this.assignedTo});
}
