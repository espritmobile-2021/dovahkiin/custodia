import 'package:custodia_kids/components/head.dart';
import 'package:custodia_kids/components/textfield_widget.dart';
import 'package:custodia_kids/models/task.dart';
import 'package:custodia_kids/pages/tasks.dart';
import 'package:custodia_kids/services/task_services.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

TextEditingController _labelControl = new TextEditingController();
TextEditingController _descControl = new TextEditingController();
TextEditingController _dueDateControl = new TextEditingController();
TextEditingController _childControl = new TextEditingController();
TextEditingController _rewardControl = new TextEditingController();

String task_resp = "";

class AddTaskScreen extends StatefulWidget {
  @override
  _AddTaskScreenState createState() => _AddTaskScreenState();
}

class _AddTaskScreenState extends State<AddTaskScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Container(
          //margin: EdgeInsets.only(top: 14, left: 14, right: 14),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: [
              Head(),
              SizedBox(
                height: 30,
              ),
              _buildTitle(),
              SizedBox(
                height: 15,
              ),
              _buildDesc(),
              SizedBox(
                height: 15,
              ),
              _buildDueDate(),
              SizedBox(
                height: 15,
              ),
              _buildReward(),
              SizedBox(
                height: 15,
              ),
              _buildChild(),
              SizedBox(
                height: 15,
              ),
              CupertinoButton(
                  child: Text('Add Task'),
                  onPressed: () async {
                    Task newTask = Task(
                        taskTitle: _labelControl.text.toString(),
                        taskDesc: _descControl.text.toString(),
                        taskPoints: _rewardControl.text.toString(),
                        originalPoster: "aa",
                        assignedTo: _childControl.text.toString());
                    task_resp = await TaskSevices.addTask(newTask);
                    if (task_resp == "created task") {
                      Navigator.push(
                          context,
                          CupertinoPageRoute(
                              builder: (context) => TaskScreen()));
                      print(task_resp);
                    } else {
                      print(task_resp);
                    }
                  })
            ],
          ),
        ),
      ),
    );
  }
}

Widget _buildTitle() {
  return TextFieldWidget(
    textInputAction: TextInputAction.next,
    hintText: "label",
    obscureText: false,
    textInputType: TextInputType.text,
    controller: _labelControl,
    /*prefixIcon: Icon(
      FeatherIcons.atSign,
      color: UIColors.primary_a,
    ),*/
    maxLines: 1,
  );
}

Widget _buildDesc() {
  return TextFieldWidget(
    textInputAction: TextInputAction.next,
    hintText: "desc",
    obscureText: false,
    textInputType: TextInputType.text,
    controller: _descControl,
    /*prefixIcon: Icon(
      FeatherIcons.atSign,
      color: UIColors.primary_a,
    ),*/
    maxLines: 1,
  );
}

Widget _buildDueDate() {
  return TextFieldWidget(
    textInputAction: TextInputAction.next,
    hintText: "due date",
    obscureText: false,
    textInputType: TextInputType.text,
    controller: _dueDateControl,
    /*prefixIcon: Icon(
      FeatherIcons.atSign,
      color: UIColors.primary_a,
    ),*/
    maxLines: 1,
  );
}

Widget _buildChild() {
  return TextFieldWidget(
    textInputAction: TextInputAction.next,
    hintText: "child",
    obscureText: false,
    textInputType: TextInputType.text,
    controller: _childControl,
    /*prefixIcon: Icon(
      FeatherIcons.atSign,
      color: UIColors.primary_a,
    ),*/
    maxLines: 1,
  );
}

Widget _buildReward() {
  return TextFieldWidget(
    textInputAction: TextInputAction.next,
    hintText: "reward",
    obscureText: false,
    textInputType: TextInputType.number,
    controller: _rewardControl,
    /*prefixIcon: Icon(
      FeatherIcons.atSign,
      color: UIColors.primary_a,
    ),*/
    maxLines: 1,
  );
}
