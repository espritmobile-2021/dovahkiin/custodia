import 'dart:convert';

import 'package:custodia_kids/models/task.dart';
import 'package:http/http.dart' as http;

class TaskSevices {
  static const String url = 'http://127.0.0.1:8000/';
  static Future addTask(Task task) async {
    final http.Response response = await http.post(
        url + "addTask/" + task.originalPoster + "/" + task.assignedTo,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{
          "lable": task.taskTitle,
          "description": task.taskDesc,
          "category": "", //useless
          "reward": task.taskPoints, //conv to int
          "created_at": task.createdAt,
          "dueDate": task.dueDate,
          "colorVal": "" //useless
        }));

    if (response.statusCode == 201) {
      return "created task";
    } else {
      return "failed to create" + response.reasonPhrase.toString();
    }
  }

  static Future<List<dynamic>> getAllTasks() async {
    final http.Response response = await http.get(
      url + "getAllTasks/",
    );
    List<dynamic> listOfTasks = json.decode(response.body);

    if (response.statusCode == 201) {
      return listOfTasks;
    } else {
      return listOfTasks = [];
    }
  }

  static Future delTask(int idTsk) async {
    final http.Response response = await http.delete(
      url + "addTask/" + idTsk.toString(),
    );
    return response;
    /*headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },*/
    /*body: jsonEncode(<String, String>{
          "lable": task.taskTitle,
          "description": task.taskDesc,
          "category": "", //useless
          "reward": task.taskPoints, //conv to int
          "created_at": task.createdAt,
          "dueDate": task.dueDate,
          "colorVal": "" //useless
        }));*/

    /*if (response.statusCode == 201) {
      return "created task";
    } else {
      return "failed to create" + response.reasonPhrase.toString();
    }*/
  }
}
