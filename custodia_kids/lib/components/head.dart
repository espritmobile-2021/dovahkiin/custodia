import 'package:custodia_kids/models/device_data.dart';
import 'package:custodia_kids/models/user.dart';
import 'package:custodia_kids/services/auth_services.dart';
import 'package:custodia_kids/utils/UIColors.dart';
import 'package:custodia_kids/utils/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';

User loggedInParent = new User(
    fullname: "Anis",
    avatarUrl: "assets/images/p1.png",
    email: "anis@mail.com");

class Head extends StatefulWidget {
  @override
  _HeadState createState() => _HeadState();
}

class _HeadState extends State<Head> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 14, left: 14, right: 14),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          FutureBuilder<User>(
              future: AuthServices.getUserObj(STATICS.CURR_LOGGED_USR),
              builder: (context, AsyncSnapshot<User> snapshot) {
                if (snapshot.hasData) {
                  DeviceData deviceData = DeviceData(
                      isWifiActive: false,
                      isLocationActive: false,
                      isSoundActive: false,
                      batteryLevel: 0);
                  User currentParent = User(
                      avatarUrl: "",
                      fullname: snapshot.data.fullname,
                      deviceData: deviceData);
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      /*Icon(CupertinoIcons.bell),
                  SizedBox(
                    width: 16,
                  ),*/
                      Text(
                        'Welcome, ${currentParent.fullname}',
                        style:
                            TextStyle(color: Colors.green[600], fontSize: 17),
                      ),
                      Spacer(),
                      InkResponse(
                        onTap: () {
                          triggerProfileBottomSheet(context);
                        },
                        child: CircleAvatar(
                            radius: 22.0,
                            backgroundColor: Colors.orange,
                            child: Image.asset(loggedInParent.avatarUrl)),
                      ),
                    ],
                  );
                } else {
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Text(
                        '...',
                        style:
                            TextStyle(color: Colors.green[600], fontSize: 20),
                      ),
                      Spacer(),
                      InkResponse(
                        onTap: () {
                          triggerProfileBottomSheet(context);
                        },
                        child: CircleAvatar(
                          radius: 22.0,
                          backgroundColor: Colors.orange,
                        ),
                      )
                    ],
                  );
                }
              }),
          SizedBox(
            height: 12,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: [
              Icon(
                FeatherIcons.info,
                color: UIColors.primaryTextColor,
                size: 18,
              ),
              SizedBox(
                width: 12,
              ),
              Text(
                'trial period expires in',
                style: TextStyle(color: UIColors.primaryTextColor),
              ),
              SizedBox(
                width: 4,
              ),
              Text(
                '30 days',
                style: TextStyle(
                    color: UIColors.primaryTextColor,
                    fontWeight: FontWeight.bold),
              ),
              /*SlideCountdownClock(
                duration: Duration(
                  days: 30,
                ),
                slideDirection: SlideDirection.Up,
                separator: ":",
                textStyle: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
                shouldShowDays: false,
                onDone: () {
                  _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text('Clock 1 finished')));
                },
              ),*/
              Spacer(),
              /*ElevatedButton(
                style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all<Color>(Colors.white)),
                onPressed: () {
                  trriggerPremiumBottomsheet(context);
                },
                child: Text(
                  "GO PREMIUM",
                  style: TextStyle(
                      color: UIColors.primary_a, fontWeight: FontWeight.w600),
                ),
              )*/
            ],
          ),
        ],
      ),
    );
  }
}

void trriggerPremiumBottomsheet(context) {
  showModalBottomSheet(
      context: context,
      builder: (context) {
        return StatefulBuilder(
            builder: (BuildContext ctx, StateSetter stateSetter) {
          return Container(
            color: Colors.white,
            child: Column(
              children: <Widget>[
                new Container(
                  decoration: BoxDecoration(
                    color: Colors.black12,
                    borderRadius: BorderRadius.all(Radius.circular(64.0)),
                  ),
                  margin: EdgeInsets.only(
                      left: 180.0, right: 180.0, top: 8.0, bottom: 14.0),
                  height: 6.0,
                ),
                Image.asset(
                  "assets/images/warning.png",
                  height: 256,
                  width: 256,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "your 30 days trial period has ended.",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    )
                  ],
                ),
                SizedBox(
                  height: 13,
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Flexible(
                        child: Text(
                          "thank you for using Custudia. We are letting you know that your free trail has expired and to continue enjoying the benefits of Custodia select a plan and become a PREMIUM MEMBER.",
                          style: TextStyle(
                            color: Colors.grey,
                            fontWeight: FontWeight.w300,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 24,
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 14.0),
                  child: Row(
                    children: [
                      Expanded(
                        child: CupertinoButton(
                          padding: EdgeInsets.symmetric(vertical: 16.0),
                          onPressed: () {
                            print('form submitted!');
                          },
                          color: UIColors.primary_a,
                          child: Text(
                            'CHECK OUR PLANS',
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                new Container(
                  height: 4.0,
                ),
              ],
            ),
          );
        });
      });
}

void triggerProfileBottomSheet(context) {
  showModalBottomSheet(
      context: context,
      builder: (context) {
        return StatefulBuilder(
            builder: (BuildContext ctx, StateSetter stateSetter) {
          return Container(
            margin: EdgeInsets.only(bottom: 14),
            height: 160,
            color: Colors.white,
            child: Column(
              children: <Widget>[
                new Container(
                  decoration: BoxDecoration(
                    color: Colors.black12,
                    borderRadius: BorderRadius.all(Radius.circular(64.0)),
                  ),
                  margin: EdgeInsets.only(
                      left: 180.0, right: 180.0, top: 8.0, bottom: 8.0),
                  height: 6.0,
                ),
                ListTile(
                  onTap: () {},
                  leading: Image.asset(
                    "assets/images/crown.png",
                    height: 30,
                    width: 30,
                  ),
                  title: Text(
                    "Upgrade to a premium member",
                    style: TextStyle(
                        color: UIColors.primaryTextColor,
                        fontWeight: FontWeight.bold,
                        fontSize: 13),
                  ),
                  trailing: Icon(
                    CupertinoIcons.arrow_right_circle,
                    color: UIColors.primaryTextColor,
                    size: 22,
                  ),
                ),
                SizedBox(
                  height: 16,
                ),
                ListTile(
                  onTap: () {},
                  leading: Icon(
                    FeatherIcons.logOut,
                    color: Colors.red,
                    size: 26,
                  ),
                  title: Text(
                    "Log out",
                    style: TextStyle(color: Colors.red),
                  ),
                ),
              ],
            ),
          );
        });
      });
}
