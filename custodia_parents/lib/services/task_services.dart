import 'dart:convert';

import 'package:custodia_parents/models/task.dart';
import 'package:custodia_parents/utils/constants.dart';
import 'package:http/http.dart' as http;

class TaskSevices {
  static const String url = 'http://127.0.0.1:8000/';
  static Future addTask(Task task) async {
    final http.Response response = await http.post(
        url + "addTask/" + task.originalPoster + "/" + task.assignedTo,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{
          "lable": task.taskTitle,
          "description": task.taskDesc,
          "category": "", //useless
          "reward": task.taskPoints, //conv to int
          "created_at": task.createdAt,
          "dueDate": task.dueDate,
          "colorVal": "" //useless
        }));

    if (response.statusCode == 201) {
      return "created task";
    } else {
      return "failed to create" + response.reasonPhrase.toString();
    }
  }

  static Future<List<dynamic>> getAllTasksByParent() async {
    final http.Response response =
        await http.get(url + "getTasksByParentMail/" + STATICS.CURR_LOGGED_USR);
    List<dynamic> listOfTasks = json.decode(response.body);

    if (response.statusCode == 201) {
      return listOfTasks;
    } else {
      return listOfTasks = [];
    }
  }

  static Future<List<dynamic>> getAllInReviewByParent() async {
    final http.Response response = await http
        .get(url + "getDoingTasksByParent/" + STATICS.CURR_LOGGED_USR);
    List<dynamic> listOfTasks = json.decode(response.body);

    if (response.statusCode == 200) {
      return listOfTasks;
    } else {
      return listOfTasks = [];
    }
  }

  static Future<List<dynamic>> getAllCompletedByParent() async {
    final http.Response response =
        await http.get(url + "getDoneTasksByParent/" + STATICS.CURR_LOGGED_USR);
    List<dynamic> listOfTasks = json.decode(response.body);

    if (response.statusCode == 200) {
      return listOfTasks;
    } else {
      return listOfTasks = [];
    }
  }

  static Future<List<dynamic>> getAllIdleByParent() async {
    final http.Response response =
        await http.get(url + "getTodoTasksByParent/" + STATICS.CURR_LOGGED_USR);
    List<dynamic> listOfTasks = json.decode(response.body);

    if (response.statusCode == 200) {
      return listOfTasks;
    } else {
      return listOfTasks = [];
    }
  }

  static Future<List<dynamic>> getAllTasks() async {
    final http.Response response = await http.get(
      url + "getAllTasks/",
    );
    List<dynamic> listOfTasks = json.decode(response.body);

    if (response.statusCode == 201) {
      return listOfTasks;
    } else {
      return listOfTasks = [];
    }
  }

  static Future<List<dynamic>> getInReviewTasks() async {
    final http.Response response = await http.get(
      url + "getDoingTasks/",
    );
    List<dynamic> listOfTasks = json.decode(response.body);

    if (response.statusCode == 201) {
      return listOfTasks;
    } else {
      return listOfTasks = [];
    }
  }

  static Future<List<dynamic>> getCompletedTasks() async {
    final http.Response response = await http.get(
      url + "getDoneTasks/",
    );
    List<dynamic> listOfTasks = json.decode(response.body);

    if (response.statusCode == 201) {
      return listOfTasks;
    } else {
      return listOfTasks = [];
    }
  }

  static Future<List<dynamic>> getIdleTasks() async {
    final http.Response response = await http.get(
      url + "getTodoTasks/",
    );
    List<dynamic> listOfTasks = json.decode(response.body);

    if (response.statusCode == 201) {
      return listOfTasks;
    } else {
      return listOfTasks = [];
    }
  }

  static Future delTask(int idTsk) async {
    final http.Response response = await http.delete(
      url + "deleteTask/" + idTsk.toString(),
    );
    return response;
  }

  static Future setCompletedtask(int idTsk) async {
    final http.Response response = await http.put(
      url + "setTaskToCompleted/" + idTsk.toString(),
    );
    return response;
  }
}
