import 'dart:convert';

import 'package:custodia_parents/models/user.dart';
import 'package:custodia_parents/utils/constants.dart';
import 'package:http/http.dart' as http;

class ChildServices {
  static const String url = 'http://127.0.0.1:8000/';
  static Future addChild(User child, String hash) async {
    final http.Response response = await http.post(url + "child/",
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, dynamic>{
          'emailParent': STATICS.CURR_LOGGED_USR,
          'email': child.fullname + '.' + child.phoneNumber + "@custodia.com",
          'password': hash,
          'fullname': child.fullname,
          'phone': child.phoneNumber,
          'role': "CHILD",
          'birthDate': "",
          'credits': 0
        }));

    if (response.statusCode == 201) {
      return "created";
    } else if (response.statusCode == 202) {
      return "email";
    } else if (response.statusCode == 203) {
      return "phone";
    } else {
      return "err register: " + response.reasonPhrase.toString();
    }
  }

  static Future getAllChildren() async {
    try {
      final response = await http.post(url + "children/",
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
          },
          body: jsonEncode(<String, String>{
            'email': STATICS.CURR_LOGGED_USR,
          }));
      if (response.statusCode == 200) {
        final List<User> mChildren = usersFromJson(response.body);
        print('test 200 ok');
        return mChildren;
      } else {
        print('test fil else');
        return List<User>();
      }
    } catch (e) {
      print(e);
    }
    //return mChildren;
  }

  /*
  static Future<List<Message>> getMsgs(String from, String to) async {
    try {
      final response = await http.post(url,
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
          },
          body: jsonEncode(<String, String>{'from': from, 'to': to}));
      if (200 == response.statusCode) {
        final List<Message> msgs = messageFromJson(response.body);
        return msgs;
      } else {
        return List<Message>();
      }
    } catch (e) {
      return List<Message>();
    }
  }
  */

  /*static Future<List<User>> addChild(String from, String to) async {
    try {
      final response = await http.post(url,
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
          },
          body: jsonEncode(<String, String>{'from': from, 'to': to}));
      if (200 == response.statusCode) {
        final List<Message> msgs = messageFromJson(response.body);
        return msgs;
      } else {
        return List<Message>();
      }
    } catch (e) {
      return List<Message>();
    }
  }*/
}
