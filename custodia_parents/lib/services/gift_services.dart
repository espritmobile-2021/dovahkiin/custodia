import 'dart:convert';

import 'package:custodia_parents/models/gift.dart';
import 'package:custodia_parents/models/user.dart';
import 'package:custodia_parents/utils/constants.dart';
import 'package:http/http.dart' as http;

class RewardServices {
  static const String url = 'http://127.0.0.1:8000/';
  static Future addReward(Gift gift) async {
    final http.Response response = await http.post(url + "addReward/",
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, dynamic>{
          'parent': gift.parent,
          'title': gift.title,
          'price': gift.price,
        }));

    if (response.statusCode == 201) {
      return "created gift";
    } else {
      return "err gift: " + response.reasonPhrase.toString();
    }
  }

  static Future getAllRewards() async {
    try {
      final response = await http.post(url + "fetchRewards/",
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
          },
          body: jsonEncode(<String, String>{
            'parent': STATICS.CURR_LOGGED_USR,
          }));
      if (response.statusCode == 200) {
        final List<Gift> gifts = giftFromJson(response.body);
        print('test 200 ok');
        return gifts;
      } else {
        print('test fil else 2');
        return List<Gift>();
      }
    } catch (e) {
      print(e);
    }
    //return mChildren;
  }

  /*
  static Future<List<Message>> getMsgs(String from, String to) async {
    try {
      final response = await http.post(url,
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
          },
          body: jsonEncode(<String, String>{'from': from, 'to': to}));
      if (200 == response.statusCode) {
        final List<Message> msgs = messageFromJson(response.body);
        return msgs;
      } else {
        return List<Message>();
      }
    } catch (e) {
      return List<Message>();
    }
  }
  */

  /*static Future<List<User>> addChild(String from, String to) async {
    try {
      final response = await http.post(url,
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
          },
          body: jsonEncode(<String, String>{'from': from, 'to': to}));
      if (200 == response.statusCode) {
        final List<Message> msgs = messageFromJson(response.body);
        return msgs;
      } else {
        return List<Message>();
      }
    } catch (e) {
      return List<Message>();
    }
  }*/
}
