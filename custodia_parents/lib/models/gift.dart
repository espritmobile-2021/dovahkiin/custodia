/*import 'dart:convert';

List<Gift> giftFromJson(String str) =>
    List<Gift>.from(json.decode(str).map((x) => Gift.fromJson(x)));

String giftToJson(List<Gift> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Gift {
  final String id;
  final String title;
  final int price;
  final String imgPath;
  final String parent;

  Gift({this.id, this.title, this.price, this.imgPath, this.parent});

  factory Gift.fromJson(Map<String, dynamic> json) => Gift(
        id: json["id"],
        title: json["title"],
        price: json["price"],
        parent: json["parent"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "price": price,
        "parent": parent,
      };
}*/
import 'dart:convert';

List<Gift> giftFromJson(String str) =>
    List<Gift>.from(json.decode(str).map((x) => Gift.fromJson(x)));

String giftToJson(List<Gift> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Gift {
  Gift({
    this.id,
    this.title,
    this.price,
    this.parent,
  });

  int id;
  String title;
  int price;
  String parent;

  factory Gift.fromJson(Map<String, dynamic> json) => Gift(
        id: json["id"],
        title: json["title"],
        price: json["price"],
        parent: json["parent"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "price": price,
        "parent": parent,
      };
}
