import 'package:custodia_parents/components/head.dart';
import 'package:custodia_parents/pages/games/chess/chess_join.dart';
import 'package:custodia_parents/pages/games/tictactoe/join_ttt.dart';
import 'package:flutter/material.dart';
import 'package:vertical_card_pager/vertical_card_pager.dart';

class GameCenter extends StatefulWidget {
  @override
  _GameCenterState createState() => _GameCenterState();
}

class _GameCenterState extends State<GameCenter> {
  final List<String> games = [
    "TIC-TAC-TOE",
    "CHESS",
    "PACMAN",
    "SUPER MARIO",
    "RETRO",
  ];
  @override
  Widget build(BuildContext context) {
    final List<Widget> tiles = [
      Container(
        child: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          child: Image.asset("assets/images/ttt.jpg"),
        ),
      ),
      Container(
        child: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          child: Image.asset("assets/images/chess.jpg"),
        ),
      ),
      Container(
        decoration: BoxDecoration(
          color: Colors.cyan,
          borderRadius: BorderRadius.all(Radius.circular(10)),
        ),
      ),
      Container(
        decoration: BoxDecoration(
          color: Colors.blue,
          borderRadius: BorderRadius.all(Radius.circular(10)),
        ),
      ),
      Container(
        decoration: BoxDecoration(
          color: Colors.grey,
          borderRadius: BorderRadius.all(Radius.circular(10)),
        ),
      ),
    ];
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Container(
          //margin: EdgeInsets.only(top: 14, left: 14, right: 14),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: [
              Head(),
              SizedBox(
                height: 16,
              ),
              Expanded(
                  child: VerticalCardPager(
                titles: games,
                textStyle:
                    TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                images: tiles,
                onPageChanged: (page) {},
                align: ALIGN.CENTER,
                onSelectedItem: (index) {
                  if (index == 0) {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => JoinPage()));
                  } else if (index == 1) {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => JoinPageC()));
                  }
                },
              ))
            ],
          ),
        ),
      ),
    );
  }
}
