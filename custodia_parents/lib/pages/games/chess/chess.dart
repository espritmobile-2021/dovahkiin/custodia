import 'package:custodia_parents/pages/games/chess/chessStream.dart';
import 'package:custodia_parents/pages/games/chess/chess_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_stateless_chessboard/flutter_stateless_chessboard.dart';

import 'package:web_socket_channel/web_socket_channel.dart';
import 'dart:convert';

class HomePageC extends StatefulWidget {
  String gameRoomId;
  String event;
  String turn;
  final WebSocketChannel channel;

  HomePageC(
      {Key key,
      @required this.event,
      @required this.channel,
      @required this.turn,
      this.gameRoomId})
      : super(key: key);

  @override
  _HomePageCState createState() => _HomePageCState();
}

class _HomePageCState extends State<HomePageC> {
  String _fen = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1';
  String roommm;
  bool isMyTurn;
  String myTurn;
  String comment = '';

  @override
  void initState() {
    roommm = widget.gameRoomId == null ? 'no room' : widget.gameRoomId;

    myTurn = widget.turn;

    String message;
    if (widget.event == "create") {
      message = '{"event":"' + widget.event + '"}';
      isMyTurn = true;
    }

    if (widget.event == "join") {
      message = '{"event":"' + widget.event + '","roomId":"' + roommm + '"}';
      isMyTurn = false;
    }

    widget.channel.sink.add(message);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
        appBar: AppBar(
          title: Text("Random Chess"),
        ),
        body: StreamBuilder(
            stream: widget.channel.stream,
            builder: (context, snapshot) {
              print(snapshot.data);
              if (snapshot.data != null) {
                ChessMoves response =
                    ChessMoves.fromJson(jsonDecode(snapshot.data));
                print(response.toString());

                if (response.event == "welcome") {
                  roommm = response.roomId;
                } else if (response.event == "move") {
                  _fen = response.fen;
                  isMyTurn = !isMyTurn;
                  comment = getCommentFromFen(_fen);
                }
              }
              return ListView(
                children: [
                  SizedBox(
                    height: 10.0,
                  ),
                  Center(
                      child: Container(
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(35),
                        color: Colors.brown),
                    child: Text(
                      'GameRoom : ' + roommm,
                      style: TextStyle(color: Colors.white, fontSize: 24),
                    ),
                  )),
                  SizedBox(
                    height: 15.0,
                  ),
                  Center(
                      child: Text(
                    isMyTurn ? "Your turn" : "opponent's turn",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: isMyTurn ? Colors.green : Colors.red,
                        fontSize: 36),
                  )),
                  SizedBox(
                    height: 15.0,
                  ),
                  Center(
                    child: Chessboard(
                      fen: _fen,
                      size: size.width * 0.9,
                      onMove: (move) {
                        final nextFen = makeMove(_fen, {
                          'from': move.from,
                          'to': move.to,
                          'promotion': 'q',
                        });

                        if (nextFen != null && isMyTurn) {
                          _sendFen(nextFen);
                        }
                      },
                    ),
                  ),
                  SizedBox(
                    height: 15.0,
                  ),
                  Center(
                    child: Text(
                      comment,
                      style: TextStyle(
                          fontSize: 30,
                          fontWeight: FontWeight.bold,
                          color: Colors.red),
                    ),
                  )
                ],
              );
            })

        //

        );
  }

  void _sendFen(String fen) {
    String turn = getTurnFromFen(fen);
    String message = '{"event":"move","roomId":"' +
        roommm +
        '","fen":"' +
        fen +
        '","turn":"' +
        turn +
        '","turn":"' +
        widget.turn +
        '"}';
    widget.channel.sink.add(message);
  }

  @override
  void dispose() {
    widget.channel.sink.close();
    super.dispose();
  }
}
