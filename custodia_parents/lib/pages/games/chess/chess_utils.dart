import 'package:chess/chess.dart' as ch;

String makeMove(String fen, dynamic move) {
  final chess = ch.Chess.fromFEN(fen);

  if (chess.move(move)) {
    String c = chess.turn.toString();
    print(c);
    String cc = c.substring(6);
    print(cc);

    if (chess.game_over) {
      print("game over");
    }

    if (chess.in_check) {
      print("cheeeck");
    }

    return chess.fen;
  }

  return null;
}

String getTurnFromFen(String fen) {
  final chess = ch.Chess.fromFEN(fen);
  String color = chess.turn.toString();

  String turn = color.substring(6);

  return turn;
}

String getCommentFromFen(String fen) {
  final chess = ch.Chess.fromFEN(fen);
  if (chess.game_over) {
    return 'CHECKMATE !!';
  } else if (chess.in_draw) {
    return 'GAME OVER : DRAW !';
  } else if (chess.in_check) {
    return 'CHECK !!';
  } else {
    return '';
  }
}

String getRandomMove(String fen) {
  final chess = ch.Chess.fromFEN(fen);

  final moves = chess.moves();

  if (moves.isEmpty) {
    return null;
  }

  moves.shuffle();

  return moves.first;
}
