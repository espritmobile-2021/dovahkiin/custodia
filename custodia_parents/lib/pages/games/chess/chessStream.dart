import 'package:flutter/material.dart';

class ChessMoves {
  String fen;
  String turn;
  String roomId;
  String event;

  ChessMoves(this.roomId, this.fen, this.turn, this.event);

  factory ChessMoves.fromJson(dynamic json) {
    return ChessMoves(json['roomId'] as String, json['fen'] as String,
        json['turn'] as String, json['event'] as String);
  }

  Map<String, dynamic> toJson() =>
      {"fen": fen, "turn": turn, "roomId": roomId, "event": event};

  @override
  String toString() {
    return '{ ${this.roomId}, ${this.fen}, ${this.turn}, ${this.event} }';
  }
}
