import 'package:custodia_parents/components/textfield_widget.dart';
import 'package:custodia_parents/pages/games/chess/chess.dart';
import 'package:flutter/material.dart';
import 'package:web_socket_channel/io.dart';

TextEditingController _roomControl = new TextEditingController();

class JoinPageC extends StatefulWidget {
  @override
  _JoinPageCState createState() => _JoinPageCState();
}

class _JoinPageCState extends State<JoinPageC> {
  String gameRoom;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("PLAY CHESS"),
      ),
      body: Column(
        children: [
          Image.asset(
            "assets/images/chess_game.png",
            height: 80,
          ),
          SizedBox(
            height: 20,
          ),
          Container(
              margin: EdgeInsets.only(right: 14, left: 14),
              child: _buildTitle()),
          SizedBox(
            height: 14,
          ),
          Container(
            margin: EdgeInsets.only(right: 14, left: 14),
            child: Row(
              children: [
                Expanded(
                  child: Container(
                    margin: EdgeInsets.only(right: 4),
                    child: RaisedButton(
                        child: Text('Join Room'),
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => HomePageC(
                                      event: 'join',
                                      channel: IOWebSocketChannel.connect(
                                          'ws://127.0.0.1:5000/ws'),
                                      turn: 'BLACK',
                                      gameRoomId: gameRoom,
                                    )),
                          );
                          print(gameRoom);
                        }),
                  ),
                ),
                Expanded(
                  child: Container(
                    margin: EdgeInsets.only(left: 4),
                    child: RaisedButton(
                        child: Text('Create Room'),
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => HomePageC(
                                      event: 'create',
                                      channel: IOWebSocketChannel.connect(
                                          'ws://127.0.0.1:5000/ws'),
                                      turn: 'WHITE',
                                    )),
                          );
                          print('Room Created');
                        }),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}

Widget _buildTitle() {
  return TextFieldWidget(
    textInputAction: TextInputAction.next,
    hintText: "entre room ID to join",
    obscureText: false,
    textInputType: TextInputType.text,
    controller: _roomControl,
    /*prefixIcon: Icon(
      FeatherIcons.atSign,
      color: UIColors.primary_a,
    ),*/
    maxLines: 1,
  );
}
