class TicTacToeMoves {
  int index;
  String turn;
  String roomId;
  String event;

  TicTacToeMoves(this.roomId, this.index, this.turn, this.event);

  factory TicTacToeMoves.fromJson(dynamic json) {
    return TicTacToeMoves(json['roomId'] as String, json['index'] as int,
        json['turn'] as String, json['event'] as String);
  }

  Map<String, dynamic> toJson() =>
      {"index": index, "turn": turn, "roomId": roomId, "event": event};

  @override
  String toString() {
    return '{ ${this.roomId}, ${this.index}, ${this.turn}, ${this.event} }';
  }
}
