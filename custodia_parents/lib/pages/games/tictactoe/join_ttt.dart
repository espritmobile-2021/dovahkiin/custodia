import 'package:custodia_parents/components/textfield_widget.dart';
import 'package:custodia_parents/pages/games/tictactoe/ttt.dart';
import 'package:flutter/material.dart';
import 'package:web_socket_channel/io.dart';

TextEditingController _roomControl = new TextEditingController();

class JoinPage extends StatefulWidget {
  @override
  _JoinPageState createState() => _JoinPageState();
}

class _JoinPageState extends State<JoinPage> {
  String gameRoom;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("PLAY TICTACTOE"),
      ),
      body: Column(
        children: [
          Image.asset(
            "assets/images/ttt_game.png",
            height: 80,
          ),
          SizedBox(
            height: 20,
          ),
          Container(
              margin: EdgeInsets.only(right: 14, left: 14),
              child: _buildTitle()),
          SizedBox(
            height: 14,
          ),
          Container(
            margin: EdgeInsets.only(right: 14, left: 14),
            child: Row(
              children: [
                Expanded(
                  child: Container(
                    margin: EdgeInsets.only(right: 4),
                    child: RaisedButton(
                        child: Text('Join Room'),
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => TicTacToeHomePage(
                                      channel: IOWebSocketChannel.connect(
                                          'ws://127.0.0.1:6000/ws'),
                                      event: 'join',
                                      gameRoomId: gameRoom,
                                    )),
                          );
                          print(gameRoom);
                        }),
                  ),
                ),
                Expanded(
                  child: Container(
                    margin: EdgeInsets.only(left: 4),
                    child: RaisedButton(
                        child: Text('Create Room'),
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => TicTacToeHomePage(
                                    channel: IOWebSocketChannel.connect(
                                        'ws://127.0.0.1:6000/ws'),
                                    event: 'create')),
                          );
                          print('Room Created');
                        }),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

Widget _buildTitle() {
  return TextFieldWidget(
    textInputAction: TextInputAction.next,
    hintText: "entre room ID to join",
    obscureText: false,
    textInputType: TextInputType.text,
    controller: _roomControl,
    /*prefixIcon: Icon(
      FeatherIcons.atSign,
      color: UIColors.primary_a,
    ),*/
    maxLines: 1,
  );
}
