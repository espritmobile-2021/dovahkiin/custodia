import 'dart:convert';

import 'package:custodia_parents/pages/games/tictactoe/tictactoeStream.dart';
import 'package:flutter/material.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

class TicTacToeHomePage extends StatefulWidget {
  final WebSocketChannel channel;
  String turn;
  String event;
  String gameRoomId;

  TicTacToeHomePage(
      {Key key, @required this.channel, @required this.event, this.gameRoomId})
      : super(key: key);

  @override
  _TicTacToeHomePageState createState() => _TicTacToeHomePageState();
}

class _TicTacToeHomePageState extends State<TicTacToeHomePage> {
  bool oTurn = true;
  List<String> displayXO = ['', '', '', '', '', '', '', '', ''];

  int oScore = 0;
  int xScore = 0;
  int filledBoxes = 0;
  bool isMyTurn;
  String turn;
  String roommm;

  @override
  void initState() {
    roommm = widget.gameRoomId == null ? 'no room' : widget.gameRoomId;

    String message;
    if (widget.event == "create") {
      message = '{"event":"' + widget.event + '"}';
      isMyTurn = true;
      turn = 'O';
    }

    if (widget.event == "join") {
      message = '{"event":"' + widget.event + '","roomId":"' + roommm + '"}';
      isMyTurn = false;
      turn = 'X';
    }

    widget.channel.sink.add(message);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[800],
        body: SafeArea(
          child: StreamBuilder(
            stream: widget.channel.stream,
            builder: (context, snapshot) {
              if (snapshot.data != null) {
                TicTacToeMoves response =
                    TicTacToeMoves.fromJson(jsonDecode(snapshot.data));
                print(response.toString());

                if (response.event == "welcome") {
                  roommm = response.roomId;
                } else if (response.event == "move") {
                  displayXO[response.index] = response.turn;
                  filledBoxes += 1;
                  isMyTurn = !isMyTurn;
                  _checkWinner();
                }
              }

              return Padding(
                padding: const EdgeInsets.all(14.0),
                child: Column(
                  children: [
                    Center(
                        child: Text(
                      "RoomId : " + roommm,
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 24,
                          fontWeight: FontWeight.bold),
                    )),
                    Expanded(
                        child: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(22.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  'Player [X]',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 24),
                                ),
                                Text(
                                  xScore.toString(),
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 24),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(22.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  'Player [O]',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 24),
                                ),
                                Text(
                                  oScore.toString(),
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 24),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    )),
                    Expanded(
                      flex: 3,
                      child: GridView.builder(
                          itemCount: 9,
                          gridDelegate:
                              SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 3),
                          itemBuilder: (BuildContext context, int index) {
                            return GestureDetector(
                              onTap: () {
                                if (isMyTurn && displayXO[index] == '') {
                                  makeMove(index);
                                }
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                    border:
                                        Border.all(color: Colors.grey[700])),
                                child: Center(
                                  child: Text(
                                    displayXO[index],
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 40),
                                  ),
                                ),
                              ),
                            );
                          }),
                    ),
                    Expanded(child: Container()),
                  ],
                ),
              );
            },
          ),
        ));
  }

  void makeMove(int tapIndex) {
    String message = '{"event":"move","roomId":"' +
        roommm +
        '","index":' +
        tapIndex.toString() +
        ',"turn":"' +
        turn +
        '"}';
    widget.channel.sink.add(message);
  }

  void _tapped(int tapIndex) {
    setState(() {
      if (oTurn && displayXO[tapIndex] == '') {
        displayXO[tapIndex] = 'O';
        filledBoxes += 1;
      } else if (!oTurn && displayXO[tapIndex] == '') {
        displayXO[tapIndex] = 'X';
        filledBoxes += 1;
      }
      oTurn = !oTurn;
      _checkWinner();
    });
  }

  AlertDialog _checkWinner() {
    //r1
    if (displayXO[0] == displayXO[1] &&
        displayXO[0] == displayXO[2] &&
        displayXO[0] != '') {
      return _displayWinDialog(displayXO[0]);
    }
    //r2
    else if (displayXO[3] == displayXO[4] &&
        displayXO[3] == displayXO[5] &&
        displayXO[3] != '') {
      return _displayWinDialog(displayXO[3]);
    }
    //r3
    else if (displayXO[6] == displayXO[8] &&
        displayXO[6] == displayXO[7] &&
        displayXO[6] != '') {
      return _displayWinDialog(displayXO[6]);
    }
    //c1
    else if (displayXO[0] == displayXO[3] &&
        displayXO[0] == displayXO[6] &&
        displayXO[0] != '') {
      return _displayWinDialog(displayXO[0]);
    }
    //c2
    else if (displayXO[1] == displayXO[4] &&
        displayXO[1] == displayXO[7] &&
        displayXO[1] != '') {
      return _displayWinDialog(displayXO[1]);
    }
    //c3
    else if (displayXO[2] == displayXO[5] &&
        displayXO[2] == displayXO[8] &&
        displayXO[2] != '') {
      return _displayWinDialog(displayXO[2]);
    }
    //d1
    else if (displayXO[6] == displayXO[4] &&
        displayXO[6] == displayXO[2] &&
        displayXO[6] != '') {
      return _displayWinDialog(displayXO[6]);
    }
    //d2
    else if (displayXO[0] == displayXO[4] &&
        displayXO[0] == displayXO[8] &&
        displayXO[0] != '') {
      return _displayWinDialog(displayXO[0]);
    }

    //draw
    else if (filledBoxes == 9) {
      return _displayDrawDialog();
    } else {
      return null;
    }
  }

  AlertDialog _displayWinDialog(String player) {
    if (player == 'O') {
      oScore += 1;
    } else if (player == 'X') {
      xScore += 1;
    }
    _resetGame();
    return AlertDialog(
      title: Text(player + ' WON THE GAME'),
      actions: [
        ElevatedButton(
          onPressed: () {
            _resetGame();
            Navigator.of(context).pop();
          },
          child: Text('PLAY AGAIN'),
        )
      ],
    );
  }

  AlertDialog _displayDrawDialog() {
    _resetGame();

    return AlertDialog(
      title: Text('DRAW'),
      actions: [
        ElevatedButton(
          onPressed: () {
            _resetGame();
            Navigator.of(context).pop();
          },
          child: Text('PLAY AGAIN'),
        )
      ],
    );
  }

  void _resetGame() {
    for (int i = 0; i < 9; i++) {
      displayXO[i] = '';
    }
    filledBoxes = 0;
  }
}
