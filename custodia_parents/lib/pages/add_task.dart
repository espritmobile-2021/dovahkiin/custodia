import 'package:custodia_parents/components/head.dart';
import 'package:custodia_parents/components/textfield_widget.dart';
import 'package:custodia_parents/models/task.dart';
import 'package:custodia_parents/models/user.dart';
import 'package:custodia_parents/pages/tasks.dart';
import 'package:custodia_parents/services/child_services.dart';
import 'package:custodia_parents/services/task_services.dart';
import 'package:custodia_parents/utils/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';

TextEditingController _labelControl = new TextEditingController();
TextEditingController _descControl = new TextEditingController();
TextEditingController _dueDateControl = new TextEditingController();
TextEditingController _childControl = new TextEditingController();
TextEditingController _rewardControl = new TextEditingController();

String task_resp = "";
List<User> users = new List<User>();
DateTime selectedDate = DateTime.now();
int selectedBirthDateTimestamp;

class AddTaskScreen extends StatefulWidget {
  @override
  _AddTaskScreenState createState() => _AddTaskScreenState();
}

class _AddTaskScreenState extends State<AddTaskScreen> {
  User selectedUser;
  String selectedChildByEmail = '';

  @override
  void initState() {
    ChildServices.getAllChildren().then((value) {
      setState(() {
        users = value;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Container(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: [
              Head(),
              SizedBox(
                height: 30,
              ),
              Container(
                  margin: EdgeInsets.only(left: 14, right: 14),
                  child: _buildTitle()),
              SizedBox(
                height: 15,
              ),
              Container(
                  margin: EdgeInsets.only(left: 14, right: 14),
                  child: _buildDesc()),
              SizedBox(
                height: 15,
              ),
              Container(
                  margin: EdgeInsets.only(left: 14, right: 14),
                  child: _buildDateField(context)),
              SizedBox(
                height: 15,
              ),
              Container(
                  margin: EdgeInsets.only(left: 14, right: 14),
                  child: _buildReward()),
              SizedBox(
                height: 15,
              ),
              Container(
                margin: EdgeInsets.only(left: 14, right: 14),
                child: DropdownButton<User>(
                  hint: Text("Assign task to"),
                  value: selectedUser,
                  onChanged: (User value) {
                    setState(() {
                      selectedUser = value;
                      selectedChildByEmail = selectedUser.email;
                    });
                  },
                  items: users.map((User user) {
                    return DropdownMenuItem<User>(
                      value: user,
                      child: Text(
                        user.fullname,
                        style: TextStyle(color: Colors.black),
                      ),
                    );
                  }).toList(),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              CupertinoButton(
                  child: Text('Add Task'),
                  onPressed: () async {
                    Task newTask = Task(
                        taskTitle: _labelControl.text.toString(),
                        taskDesc: _descControl.text.toString(),
                        taskPoints: _rewardControl.text.toString(),
                        dueDate: _dueDateControl.text.toString(),
                        originalPoster: STATICS.CURR_LOGGED_USR,
                        assignedTo: selectedChildByEmail.toString());
                    task_resp = await TaskSevices.addTask(newTask);
                    if (task_resp == "created task") {
                      Navigator.push(
                          context,
                          CupertinoPageRoute(
                              builder: (context) => TaskScreen()));
                      print(task_resp);
                    } else {
                      print(task_resp);
                    }
                  })
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildDateField(context) {
    return TextFieldWidget(
      textInputAction: TextInputAction.next,
      hintText: "Due date",
      obscureText: false,
      textInputType: TextInputType.number,
      controller: _dueDateControl,
      maxLines: 1,
      onTapped: () {
        _selectDate(context);
      },
    );
  }

  _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: selectedDate,
      firstDate: DateTime(1900),
      lastDate: DateTime(2100),
    );
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        String yyyy = selectedDate.year.toString();
        String mm = selectedDate.month.toString();
        String dd = selectedDate.day.toString();
        String newFormatting = mm + "/" + dd + "/" + yyyy;
        _dueDateControl.text = newFormatting;
        selectedBirthDateTimestamp = selectedDate.millisecondsSinceEpoch;
      });
  }
}

Widget _buildTitle() {
  return TextFieldWidget(
    textInputAction: TextInputAction.next,
    hintText: "label",
    obscureText: false,
    textInputType: TextInputType.text,
    controller: _labelControl,
    /*prefixIcon: Icon(
      FeatherIcons.atSign,
      color: UIColors.primary_a,
    ),*/
    maxLines: 1,
  );
}

Widget _buildDesc() {
  return TextFieldWidget(
    textInputAction: TextInputAction.next,
    hintText: "desc",
    obscureText: false,
    textInputType: TextInputType.text,
    controller: _descControl,
    /*prefixIcon: Icon(
      FeatherIcons.atSign,
      color: UIColors.primary_a,
    ),*/
    maxLines: 1,
  );
}

Widget _buildDueDate() {
  return TextFieldWidget(
    textInputAction: TextInputAction.next,
    hintText: "due date",
    obscureText: false,
    textInputType: TextInputType.text,
    controller: _dueDateControl,
    /*prefixIcon: Icon(
      FeatherIcons.atSign,
      color: UIColors.primary_a,
    ),*/
    maxLines: 1,
  );
}

Widget _buildChild() {
  return TextFieldWidget(
    textInputAction: TextInputAction.next,
    hintText: "child",
    obscureText: false,
    textInputType: TextInputType.text,
    controller: _childControl,
    /*prefixIcon: Icon(
      FeatherIcons.atSign,
      color: UIColors.primary_a,
    ),*/
    maxLines: 1,
  );
}

Widget _buildReward() {
  return TextFieldWidget(
    textInputAction: TextInputAction.next,
    hintText: "reward",
    obscureText: false,
    textInputType: TextInputType.number,
    controller: _rewardControl,
    /*prefixIcon: Icon(
      FeatherIcons.atSign,
      color: UIColors.primary_a,
    ),*/
    maxLines: 1,
  );
}

/*
void triggerFlagBottomsheet(context) {
  showModalBottomSheet(
      context: context,
      builder: (context) {
        return StatefulBuilder(
            builder: (BuildContext ctx, StateSetter stateSetter) {
          return Container(
            child: Wrap(
              children: <Widget>[
                new Container(
                  decoration: BoxDecoration(
                    color: Colors.black12,
                    borderRadius: BorderRadius.all(Radius.circular(64.0)),
                  ),
                  margin: EdgeInsets.only(
                      left: 140.0, right: 140.0, top: 8.0, bottom: 6.0),
                  height: 2.0,
                ),
                new Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Container(
                      child: new Container(
                        margin: EdgeInsets.only(
                            left: 16.0, right: 16.0, top: 6.0, bottom: 6.0),
                        child: new Row(
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            Icon(
                              FeatherIcons.flag,
                              color: Colors.red[300],
                              size: 14,
                            ),
                            SizedBox(
                              width: 8.0,
                            ),
                            new Text(
                              "Select a reason",
                              style: TextStyle(
                                  color: Colors.black87,
                                  fontSize: 13.0,
                                  fontWeight: FontWeight.w400),
                            ),
                            Spacer(),
                            InkWell(
                              splashColor: Colors.red[100],
                              onTap: () {
                                Navigator.of(context).pop();
                              },
                              child: Icon(
                                FeatherIcons.x,
                                color: Colors.red[300],
                                size: 22.0,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                ListView.builder(
                  shrinkWrap: true,
                  reverse: false,
                  itemCount: flagReasons.length,
                  itemBuilder: (BuildContext context, int idx) {
                    return ListTile(
                      onTap: () {
                        stateSetter(() {
                          _selectedFlag = idx;
                        });
                      },
                      leading: idx == _selectedFlag
                          ? Icon(FeatherIcons.checkCircle,
                              color: Colors.blueAccent[700])
                          : Icon(FeatherIcons.circle, color: Colors.black87),
                      title: Text(
                        flagReasons[idx],
                        style: TextStyle(
                            color: Colors.black87,
                            fontSize: 15,
                            fontWeight: FontWeight.w300),
                      ),
                    );
                  },
                ),
                /*Visibility(
                    visible: _selectedFlag == 3 ? true : false,
                    child: Container(
                      margin:
                          EdgeInsets.symmetric(horizontal: 14, vertical: 6.0),
                      child: Material(
                        color: lightGreyColor,
                        borderRadius: BorderRadius.all(Radius.circular(8.0)),
                        child: InkWell(
                          onTap: () {
                            print('box clicked');
                          },
                          child: TextField(
                            autofocus: false,
                            textInputAction: TextInputAction.done,
                            cursorColor: Colors.indigo,
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.symmetric(
                                  horizontal: 10.0, vertical: 12.0),
                              border: InputBorder.none,
                              hintText: 'why?',
                              hintStyle: TextStyle(
                                fontWeight: FontWeight.w300,
                                fontSize: 14.0,
                              ),
                              hintMaxLines: 1,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),*/
                SizedBox(
                  height: 8.0,
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 14.0, vertical: 4.0),
                  child: Row(
                    children: [
                      Expanded(
                        child: CupertinoButton(
                          padding: EdgeInsets.symmetric(vertical: 16.0),
                          onPressed: () {
                            print('form submitted!');
                          },
                          color: Colors.red[300],
                          child: Text(
                            'SEND REPORT',
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 14.0,
                ),
              ],
            ),
          );
        });
      });
}
*/
