import 'package:custodia_parents/components/head.dart';
import 'package:custodia_parents/components/textfield_widget.dart';
import 'package:custodia_parents/models/gift.dart';
import 'package:custodia_parents/models/task.dart';
import 'package:custodia_parents/models/user.dart';
import 'package:custodia_parents/pages/main_screen.dart';
import 'package:custodia_parents/pages/tasks.dart';
import 'package:custodia_parents/services/child_services.dart';
import 'package:custodia_parents/services/gift_services.dart';
import 'package:custodia_parents/services/task_services.dart';
import 'package:custodia_parents/utils/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';

TextEditingController _labelControl = new TextEditingController();
TextEditingController _rewardControl = new TextEditingController();

String task_resp = "";

class AddGiftScreen extends StatefulWidget {
  @override
  _AddGiftScreenState createState() => _AddGiftScreenState();
}

class _AddGiftScreenState extends State<AddGiftScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Container(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: [
              Head(),
              SizedBox(
                height: 30,
              ),
              Container(
                  margin: EdgeInsets.only(left: 14, right: 14),
                  child: _buildTitle()),
              SizedBox(
                height: 15,
              ),
              Container(
                  margin: EdgeInsets.only(left: 14, right: 14),
                  child: _buildReward()),
              SizedBox(
                height: 15,
              ),
              CupertinoButton(
                  child: Text('Add Gift'),
                  onPressed: () async {
                    Gift newGift = Gift(
                      title: _labelControl.text.toString(),
                      price: int.parse(_rewardControl.text),
                      parent: STATICS.CURR_LOGGED_USR,
                    );
                    task_resp = await RewardServices.addReward(newGift);
                    if (task_resp == "created gift") {
                      Navigator.push(
                          context,
                          CupertinoPageRoute(
                              builder: (context) => MainScreen()));
                      print(task_resp);
                    } else {
                      print(task_resp);
                    }
                  })
            ],
          ),
        ),
      ),
    );
  }
}

Widget _buildTitle() {
  return TextFieldWidget(
    textInputAction: TextInputAction.next,
    hintText: "label",
    obscureText: false,
    textInputType: TextInputType.text,
    controller: _labelControl,
    /*prefixIcon: Icon(
      FeatherIcons.atSign,
      color: UIColors.primary_a,
    ),*/
    maxLines: 1,
  );
}

Widget _buildReward() {
  return TextFieldWidget(
    textInputAction: TextInputAction.next,
    hintText: "credits #",
    obscureText: false,
    textInputType: TextInputType.number,
    controller: _rewardControl,
    /*prefixIcon: Icon(
      FeatherIcons.atSign,
      color: UIColors.primary_a,
    ),*/
    maxLines: 1,
  );
}
